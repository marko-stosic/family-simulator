import axios from 'axios'
import React, { Component } from 'react'

class Family extends Component {

    constructor() {
        super()
        this.state = {
            family: {},
            familyMemberList: [],
            memberList: []
        }
        this.handleAddButtonClick = this.handleAddButtonClick.bind(this)
        this.handleResetButtonClick = this.handleResetButtonClick.bind(this)
    }

    componentDidMount () {
        axios.get('/api/v1/authenticate', {
            headers: {
                'Authentication': localStorage.getItem('familyId')
            }
        }).then(response => {
            this.setState({
                family: response.data.data,
                familyMemberList: response.data.data.members
            })
            if (!localStorage.getItem('familyId')) {
                localStorage.setItem('familyId', response.data.data.id)
            }
        })
        axios.get('/api/v1/members').then(response => {
            this.setState({
                memberList: response.data.data,
            })
        })
    }

    handleAddButtonClick (event) {
        event.preventDefault()

        axios.post(`/api/v1/family/add-member/`,
            {member_id: event.target.value},
            {
                headers: {
                    'Authentication': localStorage.getItem('familyId')
                }
            }
        ).then(response => {
            if (response.data.success) {
                this.setState({
                    family: response.data.data,
                    familyMemberList: response.data.data.members
                })
            } else {
                alert('ERROR: ' + response.data.error.message)
            }

        }).catch((error) => {
            console.log(error)
        })
    }

    handleResetButtonClick (event) {
        event.preventDefault()

        axios.get(`/api/v1/family/reset/`,
            {
                headers: {
                    'Authentication': localStorage.getItem('familyId')
                }
            }
        ).then(response => {
            if (response.data.success) {
                this.setState({
                    family: response.data.data,
                    familyMemberList: response.data.data.members
                })
            } else {
                alert(error.error.message)
            }
        }).catch((error) => {
            console.log(error)
        })
    }

    render () {
        const { family } = this.state

        const addButtons = this.state.memberList.map((member, i) => {
            return (
                <button
                    onClick={this.handleAddButtonClick}
                    key={member.id}
                    value={member.id}
                >
                    Add {member.name}
                </button>
            )
        })

        const familyMembers = this.state.familyMemberList.map((member, i) => {
            return (
                <li
                    key={member.id}
                >
                    {member.member.plural_name}: {member.quantity}
                </li>
            )
        })

        let totalMembers = ''
        if (this.state.family.total_members > 0) {
            totalMembers =
                    <li key='totalMembers'>
                        <strong>Total members: {this.state.family.total_members}</strong>
                    </li>
        }
        let monthlyFoodCosts = ''
        if (this.state.family.monthly_food_costs > 0) {
            monthlyFoodCosts =
                    <li key='monthlyFoodCosts'>
                        <strong>Monthly Food Costs: {this.state.family.monthly_food_costs}</strong>
                    </li>
        }

        return (
            <div>
                {addButtons}
                <button
                    onClick={this.handleResetButtonClick}
                >
                    Reset
                </button>
                <h2>Family</h2>
                <ul>
                    {familyMembers}
                    {totalMembers}
                    {monthlyFoodCosts}
                </ul>
            </div>

        )
    }
}

export default Family

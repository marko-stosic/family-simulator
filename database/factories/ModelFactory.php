<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Enums\ValidationRule;
use App\Family;
use App\FamilyMember;
use App\Member;
use App\MemberValidation;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

$factory->define(Family::class, function (Faker $faker) {
    return [
        'id' => Str::uuid()
    ];
});

$factory->define(Member::class, function (Faker $faker) {
    return [
        'name' => $faker->firstName,
        'plural_name' => $faker->firstName,
        'monthly_cost' => $faker->numberBetween(1, 200),
        'order_nr' => $faker->unique()->numberBetween(1, 1000)
    ];
});

$factory->define(FamilyMember::class, function (Faker $faker) {
    return [
        'family_id' => DB::table('families')->inRandomOrder()->pluck('id')->first(),
        'member_id' => DB::table('members')->inRandomOrder()->pluck('id')->first(),
        'quantity' => $faker->numberBetween(1, 5)
    ];
});

$factory->define(MemberValidation::class, function (Faker $faker) {
    return [
        'member_id' => DB::table('members')->inRandomOrder()->pluck('id')->first(),
        'rule' => $faker->randomElement(ValidationRule::getValues()),
        'depends_on_members' => implode(';', DB::table('members')
            ->inRandomOrder()
            ->take($faker->numberBetween(1,3))
            ->pluck('id')
            ->toArray()),
    ];
});

<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Class MemberSeeder
 */
class MemberSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $membersData = [
            [
                'id' => 1,
                'name' => 'Mum',
                'plural_name' => 'Mum',
                'monthly_cost' => 200,
                'order_nr' => 1
            ],
            [
                'id' => 2,
                'name' => 'Dad',
                'plural_name' => 'Dad',
                'monthly_cost' => 200,
                'order_nr' => 2
            ],
            [
                'id' => 3,
                'name' => 'Child',
                'plural_name' => 'Children',
                'monthly_cost' => 150,
                'order_nr' => 3
            ],
            [
                'id' => 4,
                'name' => 'Cat',
                'plural_name' => 'Cats',
                'monthly_cost' => 10,
                'order_nr' => 5
            ],
            [
                'id' => 5,
                'name' => 'Dog',
                'plural_name' => 'Dogs',
                'monthly_cost' => 15,
                'order_nr' => 6
            ],
            [
                'id' => 6,
                'name' => 'Goldfish',
                'plural_name' => 'Goldfishes',
                'monthly_cost' => 2,
                'order_nr' => 7
            ],
            [
                'id' => 7,
                'name' => 'Adapt Child',
                'plural_name' => 'Adapt Children',
                'monthly_cost' => 150,
                'order_nr' => 4
            ],
        ];

        foreach ($membersData as $memberData) {
            $memberData['created_at'] = Carbon::now();
            $memberData['updated_at'] = Carbon::now();
            DB::table('members')->insert($memberData);
        }
    }
}

<?php

use Carbon\Carbon;
use App\Enums\ValidationRule;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Class MemberValidationSeeder
 */
class MemberValidationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $memberValidationsData = [
            // Mum validation
            [
                'member_id' => 1,
                'rule' => ValidationRule::MUST_NOT_EXIST_ONE,
                'depends_on_members' => '1'
            ],
            // Dad validation
            [
                'member_id' => 2,
                'rule' => ValidationRule::MUST_NOT_EXIST_ONE,
                'depends_on_members' => '2'
            ],
            // Child validation
            [
                'member_id' => 3,
                'rule' => ValidationRule::MUST_EXIST_ALL,
                'depends_on_members' => "1;2"
            ],
            // Cat validation
            [
                'member_id' => 4,
                'rule' => ValidationRule::MUST_EXIST_ONE,
                'depends_on_members' => "1;2"
            ],
            // Dog validation
            [
                'member_id' => 5,
                'rule' => ValidationRule::MUST_EXIST_ONE,
                'depends_on_members' => "1;2"
            ],
            // Goldfish validation
            [
                'member_id' => 6,
                'rule' => ValidationRule::MUST_EXIST_ONE,
                'depends_on_members' => "1;2"
            ],
            // Adapt child validation
            [
                'member_id' => 7,
                'rule' => ValidationRule::MUST_EXIST_ONE,
                'depends_on_members' => '1'
            ]
        ];

        foreach ($memberValidationsData as $memberValidationData) {
            $memberValidationData['created_at'] = Carbon::now();
            $memberValidationData['updated_at'] = Carbon::now();
            DB::table('member_validations')->insert($memberValidationData);
        }
    }
}

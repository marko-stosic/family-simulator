<?php

use App\Enums\ValidationRule;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateMemberValidationsTable
 */
class CreateMemberValidationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_validations', function (Blueprint $table) {
            $table->id();
            $table->string('member_id')->references('id')->on('members');
            $table->enum('rule', ValidationRule::getValues());
            $table->string('depends_on_members');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_validations');
    }
}

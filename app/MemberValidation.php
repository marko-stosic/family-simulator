<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class MemberValidation
 * @package App
 * @property int $id
 * @property int $member_id
 * @property int $rule
 * @property string $depends_on_members
 */
class MemberValidation extends Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $guarded = ['id'];

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /**
     * @return BelongsTo
     */
    public function member()
    {
        return $this->belongsTo(Member::class);
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    /**
     * @param $value
     */
    public function setDependsOnMembersAttribute($value)
    {
        $value = is_array($value) ? $value : [(int)$value];
        $this->attributes['depends_on_members'] = implode(';', $value);
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /**
     * @param $value
     * @return false|string[]
     */
    public function getDependsOnMembersAttribute($value)
    {
        return explode(';', $value);
    }
}

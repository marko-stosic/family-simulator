<?php

namespace App;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Family
 * @package App
 * @property int $id
 * @property-read Collection|Member[] $members
 */
class Family extends Model
{
    use Uuid;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $keyType = 'string';

    public $incrementing = false;

    protected $guarded = [];

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /**
     * @return HasMany
     */
    public function members()
    {
        return $this->hasMany(FamilyMember::class);
    }

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /**
     * @return int
     */
    public function monthlyFoodCosts()
    {
        $costs = 0;
        $this->members->map(function (FamilyMember $member) use (&$costs) {
            $costs += $member->member->monthly_cost * $member->quantity;
        });

        return $costs;
    }
}

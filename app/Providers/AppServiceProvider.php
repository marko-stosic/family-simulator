<?php

namespace App\Providers;

use App\Family;
use App\FamilyMember;
use App\Member;
use App\Transformers\FamilyMemberTransformer;
use App\Transformers\FamilyTransformer;
use App\Transformers\MemberTransformer;
use Flugg\Responder\Contracts\Transformers\TransformerResolver;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     * @throws BindingResolutionException
     */
    public function boot()
    {
        $this->registerTransformers();
    }

    /**
     * Register transformers to be used by the Flugger Responder package
     *
     * @throws BindingResolutionException
     */
    protected function registerTransformers()
    {
        $resolver = $this->app->make(TransformerResolver::class);
        $resolver->bind(Member::class, MemberTransformer::class);
        $resolver->bind(Family::class, FamilyTransformer::class);
        $resolver->bind(FamilyMember::class, FamilyMemberTransformer::class);
    }
}

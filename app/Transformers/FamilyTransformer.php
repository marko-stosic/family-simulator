<?php

namespace App\Transformers;

use App\Family;
use Flugg\Responder\Transformers\Transformer;

/**
 * Class FamilyTransformer
 * @package App\Transformers
 */
class FamilyTransformer extends Transformer
{
    /**
     * List of available relations.
     *
     * @var string[]
     */
    protected $relations = [];

    /**
     * List of autoloaded default relations.
     *
     * @var array
     */
    protected $load = [
        'members' => FamilyMemberTransformer::class
    ];

    /**
     * Transform the model.
     *
     * @param Family $family
     * @return array
     */
    public function transform(Family $family)
    {
        return [
            'id' => $family->id,
            'total_members' => (int)$family->members()->sum('quantity'),
            'monthly_food_costs' => $family->monthlyFoodCosts()
        ];
    }
}

<?php

namespace App\Transformers;

use App\Family;
use App\FamilyMember;
use Flugg\Responder\Transformers\Transformer;

class FamilyMemberTransformer extends Transformer
{
    /**
     * List of available relations.
     *
     * @var string[]
     */
    protected $relations = [
        'family' => Family::class,
        'member' => MemberTransformer::class
    ];

    /**
     * List of autoloaded default relations.
     *
     * @var array
     */
    protected $load = [
        'member' => MemberTransformer::class
    ];

    /**
     * Transform the model.
     *
     * @param FamilyMember $familyMember
     * @return array
     */
    public function transform(FamilyMember $familyMember)
    {
        return [
            'id' => $familyMember->id,
            'quantity' => $familyMember->quantity
        ];
    }
}

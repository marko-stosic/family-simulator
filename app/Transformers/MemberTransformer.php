<?php

namespace App\Transformers;

use App\Member;
use Flugg\Responder\Transformers\Transformer;

/**
 * Class MemberTransformer
 * @package App\Transformers
 */
class MemberTransformer extends Transformer
{
    /**
     * List of available relations.
     *
     * @var string[]
     */
    protected $relations = [];

    /**
     * List of autoloaded default relations.
     *
     * @var array
     */
    protected $load = [];

    /**
     * Transform the model.
     *
     * @param Member $member
     * @return array
     */
    public function transform(Member $member)
    {
        return [
            'id' => $member->id,
            'name' => $member->name,
            'plural_name' => $member->plural_name,
            'order_position' => $member->order_nr
        ];
    }
}

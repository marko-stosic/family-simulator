<?php

namespace App\Services;

use App\Enums\ValidationRule;
use App\Family;
use App\FamilyMember;
use App\Member;
use App\MemberValidation;
use Exception;
use Illuminate\Database\Eloquent\HigherOrderBuilderProxy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

/**
 * Class FamilyService
 */
class FamilyService
{
    /**
     * @var array
     */
    protected $validationErrors = [];

    /**
     * @param Request $request
     * @return Family|array[]|HigherOrderBuilderProxy|mixed
     * @throws Exception
     */
    public function addMember(Request $request)
    {
        $rules = [
            'member_id' => 'required|exists:members,id'
        ];

        $messages = [
            'member_id.required' => 'Family member must be defined.',
            //'member_id.exists' => 'Family member does not exist.'
        ];

        $data = Validator::make($request->all(), $rules, $messages)->validated();

        /** @var Family $family */
        $family = Family::query()->findOrFail($request->header('Authentication'));

        $this->validateFamilyMember($family, $data['member_id']);

        if (!empty($this->validationErrors)) {
            throw new Exception($this->validationErrors[0]);
        }

        $data['family_id'] = $family->id;

        $familyMember = FamilyMember::query()->where($data)->first() ?? new FamilyMember(array_merge($data, ['quantity' => 0]));
        $familyMember->quantity = $familyMember->quantity + 1;
        $familyMember->save();

        return $familyMember->family;
    }

    /**
     * @param Family $family
     * @param int $memberId
     */
    private function validateFamilyMember(Family $family, int $memberId)
    {
        $validationsQuery = MemberValidation::query()->where('member_id', $memberId);

        if ($validationsQuery->exists()) {
            $validations = $validationsQuery->get();
            $validations->map(function (MemberValidation $validation) use ($family) {
                /** @var Member $member */
                $member = Member::query()->find($validation->member_id);
                $dependsOnMembers = Member::query()->whereIn('id', $validation->depends_on_members);

                // Check if at least one family member from list exists
                $atLeastOneFamilyMemberExists = $family->members()
                    ->whereIn('family_members.member_id', $validation->depends_on_members)
                    ->exists();

                // Check if all family members from list exist
                $allFamilyMembersExist = true;
                foreach ($dependsOnMembers->pluck('id')->toArray() as $dependsOnMemberId) {
                    $allFamilyMembersExist = $allFamilyMembersExist &&
                        $family->members()->where('family_members.member_id', $dependsOnMemberId)->exists();
                }

                switch ($validation->rule) {
                    case ValidationRule::MUST_EXIST_ONE:
                        if (!$atLeastOneFamilyMemberExists) {
                            $this->validationErrors[] = 'No ' . $member->name . ' without a '
                                . implode(' or a ', $dependsOnMembers->pluck('name')->toArray());
                        }
                        break;
                    case ValidationRule::MUST_EXIST_ALL:
                        if (!$allFamilyMembersExist) {
                            $this->validationErrors[] = 'No ' . $member->name . ' without a '
                                . implode(' and a ', $dependsOnMembers->pluck('name')->toArray());
                        }
                        break;
                    case ValidationRule::MUST_NOT_EXIST_ONE:
                        if ($atLeastOneFamilyMemberExists) {
                            $this->validationErrors[] = 'Family already has a '
                                . implode(' or a ', $dependsOnMembers->pluck('name')->toArray());
                        }
                        break;
                    case ValidationRule::MUST_NOT_EXIST_ALL:
                        if ($allFamilyMembersExist) {
                            $this->validationErrors[] = 'Family already has a '
                                . implode(' and a ', $dependsOnMembers->pluck('name')->toArray());
                        }

                }
            });
        }
    }
}

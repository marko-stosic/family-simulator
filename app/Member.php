<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Member
 * @package App
 * @property int $id
 * @property string $name
 * @property string $plural_name
 * @property integer $monthly_cost
 * @property integer $order_nr
 * @method static Builder|Member orderBy($value)
 */
class Member extends Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $guarded = ['id'];
}

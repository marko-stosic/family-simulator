<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static MUST_EXIST_ONE()
 * @method static static MUST_EXIST_ALL()
 * @method static static MUST_NOT_EXIST_ONE()
 * @method static static MUST_NOT_EXIST_ALL()
 */
final class ValidationRule extends Enum
{
    const MUST_EXIST_ONE = 1;
    const MUST_EXIST_ALL = 2;
    const MUST_NOT_EXIST_ONE = 3;
    const MUST_NOT_EXIST_ALL = 4;
}

<?php

namespace App\Http\Controllers\V1;

use App\Family;
use Flugg\Responder\Http\Responses\SuccessResponseBuilder;
use Illuminate\Http\Request;

/**
 * Class AuthController
 * @package App\Http\Controllers\V1
 */
class AuthController
{
    /**
     * @param Request $request
     * @return SuccessResponseBuilder
     */
    public function index(Request $request)
    {
        $familyId = $request->header('Authentication');

        $family = Family::query()->find($familyId);

        if (!$familyId || !$family) {
            $family = new Family();
            $family->save();
        }

        return responder()->success($family);
    }
}

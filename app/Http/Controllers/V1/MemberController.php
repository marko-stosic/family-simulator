<?php

namespace App\Http\Controllers\V1;

use App\Member;
use Flugg\Responder\Http\Responses\SuccessResponseBuilder;

/**
 * Class MemberController
 * @package App\Http\Controllers\V1
 */
class MemberController
{
    /**
     * @return SuccessResponseBuilder
     */
    public function index()
    {
        $members = Member::orderBy('order_nr')->get();

        return responder()->success($members);
    }
}

<?php

namespace App\Http\Controllers\V1;

use App\Family;
use App\Services\FamilyService;
use Exception;
use Flugg\Responder\Http\Responses\ErrorResponseBuilder;
use Flugg\Responder\Http\Responses\SuccessResponseBuilder;
use Illuminate\Http\Request;

/**
 * Class FamilyController
 * @package App\Http\Controllers\V1
 */
class FamilyController
{
    /**
     * @param Request $request
     * @param FamilyService $familyService
     * @return ErrorResponseBuilder|SuccessResponseBuilder
     */
    public function addMember(Request $request, FamilyService $familyService)
    {
        try {
            $family = $familyService->addMember($request);
        } catch (Exception $e) {
            $error = $e->getMessage();
            return responder()->error(422, $error);
        }

        return responder()->success($family);
    }

    /**
     * @param Request $request
     * @return SuccessResponseBuilder
     * @throws Exception
     */
    public function reset(Request $request)
    {
        /** @var Family $family */
        $family = Family::query()->find($request->header('Authentication'));

        if ($family) {
            $family->members()->delete();
        }

        return responder()->success($family);
    }
}

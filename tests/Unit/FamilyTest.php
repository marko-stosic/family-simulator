<?php

namespace Tests\Unit;

use App\Family;
use App\FamilyMember;
use App\Member;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class FamilyTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @var Family
     */
    protected $family;

    public function setUp(): void
    {
        parent::setUp();

        $this->family = factory(Family::class)->create();
    }

    /**
     * @return void
     */
    public function testDoesntHaveMembers()
    {
        $this->assertFalse($this->family->members()->exists());
    }

    /**
     * @return void
     */
    public function testHasMembers()
    {
        /** @var Member $member */
        $member = factory(Member::class)->create();
        factory(FamilyMember::class)->create([
            'family_id' => $this->family->id,
            'member_id' => $member->id,
            'quantity' => 1
        ]);
        $this->assertTrue($this->family->members()->exists());
    }

    /**
     * @return void
     */
    public function testMonthlyFoodCostsZero()
    {
        $this->assertFalse((bool)$this->family->monthlyFoodCosts());
    }


    /**
     * @return void
     */
    public function testMonthlyFoodCostsPositive()
    {
        /** @var Member $member */
        $member = factory(Member::class)->create();
        factory(FamilyMember::class)->create([
            'family_id' => $this->family->id,
            'member_id' => $member->id,
            'quantity' => 1
        ]);
        $this->assertTrue((bool)$this->family->monthlyFoodCosts());
    }
}

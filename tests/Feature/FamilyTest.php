<?php

namespace Tests\Feature;

use App\Enums\ValidationRule;
use App\Family;
use App\FamilyMember;
use App\Member;
use App\MemberValidation;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Testing\TestResponse;
use Tests\TestCase;

class FamilyTest extends TestCase
{
    use RefreshDatabase;
    use DatabaseMigrations;

    /**
     * @var Family
     */
    protected $family;

    protected $member1;

    protected $member2;

    protected $member3;

    public function setUp(): void
    {
        parent::setUp();

        $this->family = factory(Family::class)->create();
        $this->member1 = $this->createMember(1);
        $this->member2 = $this->createMember(2);
        $this->member3 = $this->createMember(3);
    }

    /**
     * @return void
     */
    public function testAuthenticate()
    {
        $response = $this->json('GET','/api/v1/authenticate');

        $response->assertStatus(200);
    }

    /**
     * @return void
     */
    public function testListMembers()
    {
        $response = $this->json('GET','/api/v1/members');

        $response->assertStatus(200);
    }

    /**
     * @test
     * @dataProvider ruleProvider
     * @param $memberId
     * @param $rule
     * @param $dependsOnMember
     * @param $status
     */
    public function addMemberValidation($memberId, $rule, $dependsOnMember, $status)
    {
        factory(MemberValidation::class)->create([
            'member_id' => $memberId,
            'rule' => $rule,
            'depends_on_members' => $dependsOnMember
        ]);

        $response = $this->addFamilyMember($this->member1);

        $response->assertJsonFragment(['status' => $status]);
    }

    /**
     * @param $id
     * @return Member
     */
    private function createMember($id)
    {
        /** @var Member $member */
        $member = factory(Member::class)->create([
            'id' => $id
        ]);

        factory(FamilyMember::class)->create([
            'family_id' => $this->family->id,
            'member_id' => $member->id,
            'quantity' => 1
        ]);

        return $member;
    }

    /**
     * @param $member
     * @return TestResponse
     */
    private function addFamilyMember($member)
    {
        return $this->json(
            'POST',
            '/api/v1/family/add-member',
            [
                'member_id' => $member->id
            ],
            [
                'Authentication' => $this->family->id
            ]
        );
    }

    public function ruleProvider()
    {
        return [
            [
                'memberId' => 1,
                'rule' => ValidationRule::MUST_NOT_EXIST_ONE,
                'dependsOnMembers' => '1',
                'status' => 500
            ],
            [
                'memberId' => 1,
                'rule' => ValidationRule::MUST_NOT_EXIST_ALL,
                'dependsOnMembers' => '1',
                'status' => 500
            ],
            [
                'memberId' => 1,
                'rule' => ValidationRule::MUST_EXIST_ALL,
                'dependsOnMembers' => '2,3',
                'status' => 200
            ],
            [
                'memberId' => 1,
                'rule' => ValidationRule::MUST_EXIST_ONE,
                'dependsOnMembers' => '2,3',
                'status' => 200
            ],
            [
                'memberId' => 1,
                'rule' => ValidationRule::MUST_EXIST_ONE,
                'dependsOnMembers' => '4,5',
                'status' => 500
            ],
        ];
    }
}

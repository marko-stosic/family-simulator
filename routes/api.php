<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/v1/authenticate', 'V1\AuthController@index');
Route::get('/v1/members/', 'V1\MemberController@index');
Route::post('/v1/family/add-member', 'V1\FamilyController@addMember');
Route::get('/v1/family/reset', 'V1\FamilyController@reset');
